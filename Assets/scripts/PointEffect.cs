using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointEffect : MonoBehaviour
{
    public AudioClip meec;
    AudioSource audioSource;
    SpriteRenderer spriteRenderer;
    float time = 0.0f;
    bool on = false;
    // Start is called before the first frame update
    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        spriteRenderer.enabled = false;
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        time += Time.deltaTime;

        if (on && time > 0.1f)
        {
            spriteRenderer.enabled = !spriteRenderer.enabled;
            time = 0.0f;
        }

        if (!on) spriteRenderer.enabled = false;
        
    }

    public void On()
    {
        audioSource.PlayOneShot(meec);
        on = true;
    }

    public void Off()
    {
        on = false;
    }
}
