using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DigitController : MonoBehaviour
{
    public SpriteRenderer spriteRenderer;
    public Sprite[] sprites;
    // Start is called before the first frame update
    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        sprites = Resources.LoadAll<Sprite>("digits");

    }

    // Update is called once per frame
    public void ChangeSprite()
    {
        spriteRenderer.sprite = sprites[3];

    }

    public void SetDigit(int digit)
    {
        spriteRenderer.sprite = sprites[digit];
    }
}