using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreController : MonoBehaviour
{
    // Start is called before the first frame update

    public int score = 0;

    DigitController digit0Controller;
    DigitController digit1Controller;

    void Start()
    {
        digit0Controller = transform.GetChild(0).gameObject.GetComponent<DigitController>();
        digit1Controller = transform.GetChild(1).gameObject.GetComponent<DigitController>();

    }

    public void AddPoint()
    {
        score++;

        int digit0 = score / 10;
        int digit1 = score % 10;

        digit0Controller.SetDigit(digit0);
        digit1Controller.SetDigit(digit1);
    }
}
