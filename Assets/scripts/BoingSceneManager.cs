using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public static class BoingSceneManager
{
    public static int mode = 0; // 0 -> automatic, 1 -> one player, 2 -> two players
    

    public static void LoadScene(string name, int m)
    {
        mode = m;
        SceneManager.LoadScene(name);
    }

    public static void LoadMenu()
    {
        mode = 0;
        SceneManager.LoadScene("MenuScene");
        SceneManager.LoadScene("GameScene", LoadSceneMode.Additive);
    }
}
