using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OverController : MonoBehaviour
{

    bool Enabled { get; set; }

    SpriteRenderer spriteRenderer;
    // Start is called before the first frame update
    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        spriteRenderer.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        spriteRenderer.enabled = Enabled;  
    }

    public void Show()
    {
        Enabled = true;
    }

    public void Hide()
    {
        Enabled = false;
    }

}
