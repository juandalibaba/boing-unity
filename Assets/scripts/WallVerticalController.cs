using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallVerticalController : MonoBehaviour
{
    public GameObject effect;
    
    public void Goal()
    {
        effect.SetActive(true);
    }
}
