using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatController : MonoBehaviour
{
    Rigidbody2D rigidbody2D;
    SpriteRenderer spriteRenderer;
    float vertical;
    
    public int player;
    public float speed=10;
    public GameObject ball;
    public BallController ballController;

    public bool resetPosition = false;

    // Start is called before the first frame update
    void Start()
    {
        rigidbody2D = GetComponent<Rigidbody2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        if (ball)
        {
            ballController = ball.GetComponent<BallController>();
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        if (BoingSceneManager.mode == 0)
        {
            vertical = AIPlayer();
        }
        else if (player == 0)
        {
            vertical = Input.GetAxis("Vertical");
        }
        else if (player == 1 && BoingSceneManager.mode == 2) // two players
        {
            vertical = Input.GetAxis("Horizontal");
        }
        else if (player == 1 && BoingSceneManager.mode == 1) // one player
        {
            vertical = AIPlayer();
        }
    }

    float AIPlayer()
    {
        double correction = 0.4;
        double yCut = ballController.getYCut(player);

        if (yCut > rigidbody2D.position.y + correction * spriteRenderer.sprite.rect.height / 2)
        {
            vertical = 1.0f;
        }
        else if (yCut < rigidbody2D.position.y - correction * spriteRenderer.sprite.rect.height / 2)
        {
            vertical = -1.0f;
        }
        else
        {
            vertical = 0.0f;
        }

        return vertical;
    }

    private void FixedUpdate()
    {
        if (resetPosition)
        {
            rigidbody2D.MovePosition(new Vector2(rigidbody2D.position.x, 0));
            resetPosition = false;
            return;
        }
        Vector2 move = new Vector2(0, vertical);

        Vector2 position = rigidbody2D.position;

        if(position.y > 160)
        {
            position.y = 160;
        }
        else if(position.y < -160)
        {
            position.y = -160;
        }
        else
        {
            position = position + move * speed * Time.deltaTime;
        }
        

        rigidbody2D.MovePosition(position);
    }
}
