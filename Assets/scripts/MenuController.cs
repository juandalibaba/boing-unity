using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class MenuController : MonoBehaviour
{
    Animator animator;
    AudioSource audio;
    int mode = 0;
    public AudioClip keypress;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        audio = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            //audio.Stop();
            if (mode == 0) mode = 1;
            BoingSceneManager.LoadScene("GameScene",mode);
        }
        if (Input.GetKeyDown(KeyCode.UpArrow) && !animator.GetCurrentAnimatorStateInfo(0).IsName("menu0"))
        {
            animator.SetTrigger("one player");
            audio.PlayOneShot(keypress);
            mode = 1;

        }
        if (Input.GetKeyDown(KeyCode.DownArrow) && !animator.GetCurrentAnimatorStateInfo(0).IsName("menu1"))
        {
            animator.SetTrigger("two players");
            audio.PlayOneShot(keypress);
            mode = 2;
        }
        
    }
}
