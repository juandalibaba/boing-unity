using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{
    public float speed = 1;

    public AudioClip soundHit;

    public int maxScore = 11;

    public GameObject scoreLeft;
    public GameObject scoreRight;

    ScoreController scoreLeftController;
    ScoreController scoreRightController;

    public GameObject batLeft;
    public GameObject batRight;

    BatController batLeftController;
    BatController batRightController;

    public GameObject effectLeft;
    public GameObject effectRight;

    PointEffect effectLeftController;
    PointEffect effectRightController;
   
    public GameObject timerHitAnimation;
    BoingTimer timerHitAnimationController;

    public GameObject timerPointAnimation;
    BoingTimer timerPointAnimationController;

    public GameObject over;
    OverController overController;

    Vector2 direction;
    Rigidbody2D rigidbody2D;
    // Start is called before the first frame update
    Animator animator;

    AudioSource audioSource;

    public int status = 0; // 0 -> init, 1 -> playing, 2 -> ended
    bool startRight = true;

    void StartHitAnimation()
    {
        animator.SetTrigger("hit");
    }

    void StopHitAnimation()
    {
        animator.SetTrigger("normal");
    }

    void Start()
    { 
        rigidbody2D = GetComponent<Rigidbody2D>();
        audioSource = GetComponent<AudioSource>();
        animator = GetComponent<Animator>();
        direction = new Vector2(1,0);
        scoreLeftController = scoreLeft.GetComponent<ScoreController>();
        scoreRightController = scoreRight.GetComponent<ScoreController>();
        batLeftController = batLeft.GetComponent<BatController>();
        batRightController = batRight.GetComponent<BatController>();
        effectLeftController = effectLeft.GetComponent<PointEffect>();
        effectRightController = effectRight.GetComponent<PointEffect>();
        timerHitAnimationController = timerHitAnimation.GetComponent<BoingTimer>();
        timerPointAnimationController = timerPointAnimation.GetComponent<BoingTimer>();
        overController = over.GetComponent<OverController>();

        timerHitAnimationController.SetActions(StartHitAnimation, StopHitAnimation);

    }

    // Update is called once per frame
    void Update()
    {
        if (status == 0)
        {
            rigidbody2D.position = new Vector2(0,0);
            direction = startRight ? new Vector2(1, 0) : new Vector2(-1, 0);
            direction = direction.normalized;
            batLeftController.resetPosition = true;
            batRightController.resetPosition = true;
            status = 1;
        }

        if (status == 1)
        {
            rigidbody2D.velocity = direction * speed;
        }

        if (status == 2)
        {
            overController.Show();

            if (Input.GetKeyDown(KeyCode.Space))
            {
                BoingSceneManager.LoadMenu();
            }
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }

    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.name.Contains("WallVerticalLeft"))
        {
            if(scoreLeftController.score == maxScore - 1)
            {
                status = 2; 
                return;
            }
            scoreLeftController.AddPoint();
            
            timerPointAnimationController.SetActions(effectLeftController.On, effectLeftController.Off);
            timerPointAnimationController.StartTimer(0.30f);
            startRight = false;
            status = 0;
        }

        if (other.gameObject.name.Contains("WallVerticalRight"))
        {
            if (scoreRightController.score == maxScore - 1)
            {
                status = 2;
                return;
            }
            scoreRightController.AddPoint();
            effectLeftController.Off();
            timerPointAnimationController.SetActions(effectRightController.On, effectRightController.Off);
            timerPointAnimationController.StartTimer(0.30f);
            startRight = true;
            status = 0;
        }

        if (other.gameObject.name.Contains("WallHorizontal"))
        {
            direction.y = -direction.y;
        }

        if (other.collider.GetComponent<BatController>())
        {
            audioSource.PlayOneShot(soundHit);
            //startTimer(0.10f);
            timerHitAnimationController.StartTimer(0.10f);
            direction.x = -direction.x;
            float deviation = Random.Range(0.0f, 0.5f);
            direction.y = direction.y + deviation;
            //direction = direction.normalized;
        }
    }

    public double getYCut(int player)
    {
        //self.y_cut = self.ball.center_y + (w - self.ball.center_x) * self.ball.change_y / self.ball.change_x
        double yb = rigidbody2D.position.y;
        double xb = rigidbody2D.position.x;
        double vx = rigidbody2D.velocity.x;
        double vy = rigidbody2D.velocity.y;
        double xCut = (player == 0)? -360.0 : 360.0;

        double yCut = yb + (vy / vx) * (xCut - xb);

        return yCut;
    }
}
